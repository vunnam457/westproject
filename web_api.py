FROM python:2.7
mkdir web_api
COPY . /web_api
WORKDIR /web_api
RUN pip install -r requirements.txt
EXPOSE 443
ENTRYPOINT ["python"]
CMD ["web_api.py", "-p 443"]