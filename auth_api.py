FROM python:2.7
COPY . /auth-api
WORKDIR /auth-app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["auth_api.py"]